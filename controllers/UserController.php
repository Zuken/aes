<?php

namespace app\controllers;

use app\models\User;
use app\models\UserSearch;
use app\models\UserForm;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\Response;
use Yii;

/**
 * Управляет работой с пользователями
 *
 * @package app\controllers
 */
class UserController extends \yii\web\Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors(){
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'restore'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Отображает список пользователей
     *
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->can('listUsers')) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', compact('dataProvider', 'searchModel'));
    }

    /**
     * Отображает страницу создания пользователя
     *
     * @return string|Response
     * @throws \yii\base\Exception
     */
    public function  actionCreate()
    {
        if (!Yii::$app->user->can('createUser')) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }
        $userForm = new UserForm(['scenario' => UserForm::SCENARIO_CREATE]);
        $userForm->defaultType();
        if ($userForm->load(Yii::$app->request->post()) && $userForm->create()) {
            Yii::$app->session->setFlash('success', 'Пользователь успешно создан');
            return Yii::$app->response->redirect('/users');
        }
        return $this->render('create', ['userForm' => $userForm]);
    }

    /**
     * Отображает страницу с информацией о пользователе
     *
     * @param $id
     * @return string
     * @throws ForbiddenHttpException|NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        if (!Yii::$app->user->can('viewUser', ['user' => $model])) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }
        $dataProvider = null;

        if ($model->type == 'student') {
            $dataProvider = new ActiveDataProvider([
                'query' => $model->students->getAttendances()
            ]);
        }


        return $this->render('view', [
            'model' => $model,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Отображает страницу редактирования пользователя
     *
     * @param $id
     * @return string|Response
     * @throws \yii\base\Exception
     */
    public function actionUpdate($id)
    {
        $userForm = new UserForm(['id' => $id, 'scenario' => UserForm::SCENARIO_UPDATE]);
        if (!Yii::$app->user->can('updateUser', ['user' => $userForm])) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }
        if ($userForm->load(Yii::$app->request->post()) && $userForm->update()) {
            Yii::$app->session->setFlash('success', 'Пользователь успешно изменен');
            return Yii::$app->response->redirect("/user/$id");
        }

        return $this->render('update', ['userForm' => $userForm]);
    }

    /**
     * Удаляет пользователя
     *
     * @return Response
     * @throws ForbiddenHttpException|NotFoundHttpException
     */
    public function actionDelete()
    {
        if (!Yii::$app->user->can('deleteUser')) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }
        $id = Yii::$app->request->post('id');
        $user = $this->findModel($id);

        if (!$user->deleted) {
            $user->deleted = true;
            if (!$user->save()) {
                Yii::$app->session->setFlash('error', 'Во время удаления произошла ошибка');
            } else {
                Yii::$app->session->setFlash('success', 'Пользователь успешно удален');
            }
        }

        return Yii::$app->response->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : '/users');
    }

    /**
     * Восстанавливает пользователя
     *
     * @return Response
     * @throws ForbiddenHttpException|NotFoundHttpException
     */
    public function actionRestore()
    {
        if (!Yii::$app->user->can('restoreUser')) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }
        $id = Yii::$app->request->post('id');
        $user = $this->findModel($id);

        if ($user->deleted) {
            $user->deleted = false;
            if (!$user->save()) {
                Yii::$app->session->setFlash('error', 'Во время восстановления произошла ошибка');
            } else {
                Yii::$app->session->setFlash('success', 'Пользователь успешно восстановлен');
            }
        }

        return Yii::$app->response->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : '/users');
    }


    /**
     * Ищет пользователя по id
     *
     * @param integer $id
     * @return User
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('Страница не найдена.');
    }
}
