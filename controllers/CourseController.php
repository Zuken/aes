<?php

namespace app\controllers;

use Yii;
use app\models\Course;
use app\models\CourseSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

/**
 * Управляет работой с курсами
 *
 * @package app\controllers
 */
class CourseController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors(){
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'restore'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Отображает список курсов
     *
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->can('listCourses')) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }
        $searchModel = new CourseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', compact('searchModel','dataProvider'));
    }

    /**
     * Отображает страницу просмотра курса
     *
     * @param integer $id
     * @return string
     * @throws NotFoundHttpException|ForbiddenHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        if (!Yii::$app->user->can('viewCourse')) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Отображает страницу создания курса
     *
     * @return string|Response
     * @throws ForbiddenHttpException
     */
    public function actionCreate()
    {
        if (!Yii::$app->user->can('createCourse')) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }
        $model = new Course();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Отображает страницу редактирования курса
     *
     * @param integer $id
     * @return string|Response
     * @throws NotFoundHttpException|ForbiddenHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (!Yii::$app->user->can('updateCourse')) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Удаляет курс
     *
     * @return Response
     * @throws NotFoundHttpException|ForbiddenHttpException
     */
    public function actionDelete()
    {
        if (!Yii::$app->user->can('deleteCourse')) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }
        $id = Yii::$app->request->post('id');
        $course = $this->findModel($id);

        if (!$course->deleted) {
            $course->deleted = true;
            if (!$course->save()) {
                Yii::$app->session->setFlash('error', 'Во время удаления произошла ошибка');
            } else {
                Yii::$app->session->setFlash('success', 'Курс успешно удален');
            }
        }

        return Yii::$app->response->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : '/courses');
    }

    /**
     * Восстанавливает курс
     *
     * @return Response
     * @throws NotFoundHttpException|ForbiddenHttpException
     */
    public function actionRestore()
    {
        if (!Yii::$app->user->can('restoreCourse')) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }
        $id = Yii::$app->request->post('id');
        $group = $this->findModel($id);

        if ($group->deleted) {
            $group->deleted = false;
            if (!$group->save()) {
                Yii::$app->session->setFlash('error', 'Во время восстановления произошла ошибка');
            } else {
                Yii::$app->session->setFlash('success', 'Курс успешно восстановлен');
            }
        }

        return Yii::$app->response->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : '/courses');
    }

    /**
     * Ищет курс по id
     *
     * @param integer $id
     * @return Course
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = Course::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('Страница не найдена.');
    }
}
