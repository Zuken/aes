<?php

namespace app\controllers;

use Yii;
use app\models\Group;
use app\models\GroupSearch;
use app\models\Student;
use yii\web\Controller;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\data\ActiveDataProvider;

/**
 * GroupController implements the CRUD actions for Group model.
 */
class GroupController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors(){
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'restore'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Отображает список групп
     *
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->can('listGroups')) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }
        $searchModel = new GroupSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Отображает группу по id
     *
     * @param integer $id
     * @return string
     * @throws NotFoundHttpException|ForbiddenHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        if (!Yii::$app->user->can('viewGroup', ['group' => $model])) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }
        $dataGroupStudents = new ActiveDataProvider([
            'query' => Student::find()->where(['group_id' => $id]),
        ]);

        return $this->render('view', [
            'model' => $model,
            'dataGroupStudents' => $dataGroupStudents,
        ]);
    }

    /**
     * Создает новую группу
     *
     * @return string|Response
     * @throws \yii\db\Exception|ForbiddenHttpException
     */
    public function actionCreate()
    {
        if (!Yii::$app->user->can('createGroup')) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }
        $model = new Group();
        $dataFreeStudents = new ActiveDataProvider([
            'query' => Student::find()->where(['group_id' => NULL]),
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($model->setGroups(Yii::$app->request->post('students'), $model->id)) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'dataFreeStudents' => $dataFreeStudents,
        ]);
    }

    /**
     * Обновляет существующую группу
     *
     * @param integer $id
     * @return string|Response
     * @throws NotFoundHttpException|ForbiddenHttpException
     * @throws \yii\db\Exception
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (!Yii::$app->user->can('updateGroup')) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }

        $dataGroupStudents = new ActiveDataProvider([
            'query' => Student::find()->where(['group_id' => $id]),
        ]);
        $dataFreeStudents = new ActiveDataProvider([
            'query' => Student::find()->where(['group_id' => NULL]),
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($model->setGroups(Yii::$app->request->post('studentGroup'), 'null')
                && $model->setGroups(Yii::$app->request->post('newStudent'), $model->id)) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'dataFreeStudents' => $dataFreeStudents,
            'dataGroupStudents' => $dataGroupStudents,
        ]);
    }

    /**
     * Удаляет группу
     *
     * @return Response
     * @throws NotFoundHttpException|ForbiddenHttpException
     */
    public function actionDelete()
    {
        if (!Yii::$app->user->can('deleteGroup')) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }
        $id = Yii::$app->request->post('id');
        $group = $this->findModel($id);

        if (!$group->deleted) {
            $group->deleted = true;
            if (!$group->save()) {
                Yii::$app->session->setFlash('error', 'Во время удаления произошла ошибка');
            } else {
                Yii::$app->session->setFlash('success', 'Группа успешно удалена');
            }
        }

        return Yii::$app->response->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : '/groups');
    }

    /**
     * Восстанавливает группу
     *
     * @return Response
     * @throws NotFoundHttpException|ForbiddenHttpException
     */
    public function actionRestore()
    {
        if (!Yii::$app->user->can('restoreGroup')) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }
        $id = Yii::$app->request->post('id');
        $group = $this->findModel($id);

        if ($group->deleted) {
            $group->deleted = false;
            if (!$group->save()) {
                Yii::$app->session->setFlash('error', 'Во время восстановления произошла ошибка');
            } else {
                Yii::$app->session->setFlash('success', 'Группа успешно восстановлена');
            }
        }

        return Yii::$app->response->redirect(Yii::$app->request->referrer ? Yii::$app->request->referrer : '/groups');
    }

    /**
     * Ищет группу по id
     *
     * @param integer $id
     * @return Group
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = Group::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('Страница не найдена.');
    }
}
