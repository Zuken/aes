<?php

namespace app\controllers;

use Yii;

use app\models\AttendanceForm;
use app\models\Group;
use app\models\Attendance;
use app\models\AttendanceSearch;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;

/**
 * Управляет работой с записями посещений
 */
class AttendanceController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors(){
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'save'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Отображает список всех посещенмй
     *
     * @return string
     * @throws ForbiddenHttpException
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->can('listAttendances')) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }
        $searchModel = new AttendanceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Отображает страницу создания записи посещений
     *
     * @return string
     * @throws NotFoundHttpException|ForbiddenHttpException
     */
    public function actionCreate()
    {
        $groupId = Yii::$app->request->post('groupId');
        $group = Group::findOne($groupId);

        if (!Yii::$app->user->can('createAttendance', ['group' => $group, 'createAttendance' => true])) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }

        if (!$group) {
            throw new NotFoundHttpException('Страница не найдена.');
        }

        $model = new AttendanceForm(['groupId' => $groupId, 'groupName' => $group->name]);
        $dataProvider = new ActiveDataProvider([
            'query' => $group->getStudents(),
        ]);

        return $this->render('create', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Обновляет запись посещаемости
     *
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException|ForbiddenHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (!Yii::$app->user->can('updateAttendance', ['attendance' => $model])) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Запись успешно сохранена');
            return Yii::$app->response->redirect(Url::to(['index']));
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Сохраняет посещаемость
     *
     * @return \yii\web\Response
     * @throws ForbiddenHttpException
     */
    public function actionSave()
    {
        $students = Yii::$app->request->post('students');
        $model = new AttendanceForm();
        if ($model->load(Yii::$app->request->post())) {
            if (!Yii::$app->user->can('createAttendance', ['group' => Group::findOne($model->groupId), 'createAttendance' => true])) {
                throw new ForbiddenHttpException('Недостаточно прав');
            }

            if ($model->save($students)) {
                Yii::$app->session->setFlash('success', 'Запись успешно сохранена');
            }
        } else {
            Yii::$app->session->setFlash('error', 'Во время работы произошла ошибка');
        }

        return Yii::$app->response->redirect(Url::to(['group/view', 'id' => $model->groupId]));
    }

    /**
     * Ищет запись посещения по id
     *
     * @param integer $id
     * @return Attendance
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = Attendance::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
