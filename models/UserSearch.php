<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;

/**
 * UsersSearch represents the model behind the search form of `app\models\User`.
 */
class UserSearch extends User
{
    /** @var string */
    public $fullName;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['username', 'type', 'fullName'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Создает data provider с запрашиваемыми параметрами
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $students = (new Query())->select(['user_id', 'lastName', 'firstName', 'middleName'])->from('students');
        $educators = (new Query())->select(['user_id', 'lastName', 'firstName', 'middleName'])->from('educators');
        $query =  User::find()->leftJoin(['fio' => $students->union($educators)], 'fio.user_id = users.id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'defaultOrder' => ['id' => SORT_DESC],
            'attributes' => [
                'id',
                'fullName' => [
                    'asc' => ['lastName' => SORT_ASC, 'firstName' => SORT_ASC, 'middleName' => SORT_ASC],
                    'desc' => ['lastName' => SORT_DESC, 'firstName' => SORT_DESC, 'middleName' => SORT_DESC],
                    'default' => SORT_ASC
                ],
                'type',
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'type', $this->type]);
        $query->andWhere('firstName LIKE "%' . $this->fullName . '%" ' .
            'OR lastName LIKE "%' . $this->fullName . '%"' .
            'OR middleName LIKE "%' . $this->fullName . '%"'
        );

        return $dataProvider;
    }
}
