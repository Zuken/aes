<?php

namespace app\models;

use Yii;
use yii\base\Model;

class UserForm extends Model
{
    /** @var integer|null  */
    public $id = null;
    /** @var string */
    public $username;
    /** @var string */
    public $nowUsername;
    /** @var string */
    public $password;
    /** @var string */
    public $repeatPassword;
    /** @var string */
    public $lastName;
    /** @var string */
    public $firstName;
    /** @var string */
    public $middleName;
    /** @var string  */
    public $type;

    /** @var string */
    const SCENARIO_CREATE = 'create';
    /** @var string  */
    const SCENARIO_UPDATE = 'update';

    public function __construct($config = [])
    {
        parent::__construct($config);
        if($this->id != null) {
            $this->find($this->id);
        }
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['username', 'lastName', 'firstName', 'middleName'], 'trim'],
            [['username', 'lastName', 'firstName'], 'required', 'message' => 'Поле обязательно для заполнения'],
            [['password', 'repeatPassword'], 'required', 'message' => 'Поле обязательно для заполнения', 'on' => 'create'],
            [['password', 'repeatPassword'], 'safe', 'on' => 'update'],
            ['username', 'string', 'min' => 5],
            [['password'], 'string', 'min' => 6],
            [['username', 'lastName', 'firstName', 'middleName', 'password'], 'string', 'max' => 20],
            ['repeatPassword', 'compare', 'compareAttribute' => 'password' , 'skipOnEmpty' => false, 'message' => 'Пароли не совпадают'],
            [['username'], 'validateUsername'],
            ['type', 'default', 'value' => 'student']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'username' => 'Имя пользователя',
            'password' => 'Пароль',
            'repeatPassword' => 'Повторите пароль',
            'lastName' => 'Фамилия',
            'firstName' => 'Имя',
            'middleName' => 'Отчество',
            'type' => 'Тип пользователя'
        ];
    }

    /**
     * Валидатор для поля Username
     *
     * @param $attribute
     */
    public function validateUsername($attribute)
    {
        if (!$this->hasErrors()) {
            if ($this->scenario == 'create') {
                goto Check;
            }
            $user = User::findOne($this->id);
            if ($user->username != $this->username) {
                Check:
                if (User::findByUsername($this->username)) {
                    $this->addError($attribute, 'Пользователь с таким именем уже существует');
                }
            }
        }
    }

    /**
     * Устанавливает значение поля "типа пользователя" поумолчанию
     */
    public function defaultType()
    {
        $this->type = User::STUDENT_TYPE;
    }

    /**
     * Добавляет пользователя в БД
     *
     * @return User|bool
     * @throws \yii\base\Exception
     */
    public function create()
    {
        if (!$this->validate()) {
            return false;
        }
        $user = new User();
        $user->username = $this->username;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->type = $this->type;

        if (!$user->save()) {
            return false;
        }
        $auth = Yii::$app->authManager;

        $newUser = new Student();
        $role = $auth->getRole('student');

        if ($user->type == User::EDUCATOR_TYPE) {
            $newUser = new Educator();
            $role = $auth->getRole('educator');
        }
        $newUser->user_id = $user->getId();
        $newUser->lastName = $this->lastName;
        $newUser->firstName = $this->firstName;
        $newUser->middleName = $this->middleName;

        if ($newUser->save()) {
            $auth->assign($role, $user->getId());
            return $user;
        }
        return false;
    }

    /**
     * Изменяет пользователя в БД
     *
     * @return bool
     * @throws \yii\base\Exception
     */
    public function update()
    {
        if (!$this->validate()) {
            return false;
        }
        $user = User::findOne($this->id);
        if ($this->username != $user->username) {
            $user->username = $this->username;
        }

        if (!empty($this->password) && !$user->validatePassword($this->password)) {
            $user->setPassword($this->password);
        }

        if (!$user->save()) {
            return false;
        }
        if ($user->type == User::STUDENT_TYPE) {
            $user->students->lastName = $this->lastName;
            $user->students->firstName = $this->firstName;
            $user->students->middleName = $this->middleName;

            return $user->students->save() ? true : false;
        } else {
            $user->educators->lastName = $this->lastName;
            $user->educators->firstName = $this->firstName;
            $user->educators->middleName = $this->middleName;

            return $user->educators->save() ? true : false;
        }
    }

    /**
     * Заполняет форму значениям пользователя(id) из БД
     *
     * @param $id
     * @return $this
     */
    public function find($id)
    {
        $user = User::findOne($id);
        $this->username = $user->username;
        $this->nowUsername = $user->username;
        $this->type = $user->type;

        if ($this->type == User::STUDENT_TYPE) {
            $this->lastName = $user->students->lastName;
            $this->firstName = $user->students->firstName;
            $this->middleName = $user->students->middleName;
        } else {
            $this->lastName = $user->educators->lastName;
            $this->firstName = $user->educators->firstName;
            $this->middleName = $user->educators->middleName;
        }
        return $this;
    }
}
