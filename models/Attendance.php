<?php

namespace app\models;

/**
 * This is the model class for table "attendance".
 *
 * @property int $id
 * @property int $student_id
 * @property string $date
 * @property bool $visited
 *
 * @property Student $students
 */
class Attendance extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'attendance';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['student_id', 'date'], 'required'],
            [['student_id'], 'integer'],
            [['visited'], 'boolean'],
            [['date'], 'safe'],
            [['student_id'], 'exist', 'skipOnError' => true, 'targetClass' => Student::className(), 'targetAttribute' => ['student_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'student_id' => 'Student ID',
            'date' => 'Дата',
            'visited' => 'Посещаемость',
            'fullName' => 'ФИО'
        ];
    }

    /**
     * Возвращает кол-во записей
     *
     * @param null $params
     * @return int|string
     */
    public static function getCount($params = null)
    {
        return static::find()->Where($params)->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudents()
    {
        return $this->hasOne(Student::className(), ['id' => 'student_id']);
    }

    /**
     * Возвращает ФИО
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->students->getFullName();
    }
}
