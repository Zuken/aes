<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CourseSearch represents the model behind the search form of `app\models\Course`.
 */
class CourseSearch extends Course
{
    /** @var string */
    public $fullName;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'educator_id'], 'integer'],
            [['name', 'fullName'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Создает data provider с запрашиваемыми параметрами
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Course::find()->joinWith('educators');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'defaultOrder' => ['id' => SORT_DESC],
            'attributes' => [
                'id',
                'name',
                'fullName' => [
                    'asc' => ['lastName' => SORT_ASC, 'firstName' => SORT_ASC, 'middleName' => SORT_ASC],
                    'desc' => ['lastName' => SORT_DESC, 'firstName' => SORT_DESC, 'middleName' => SORT_DESC],
                    'default' => SORT_ASC
                ],
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);
        $query->andWhere('firstName LIKE "%' . $this->fullName . '%" ' .
            'OR lastName LIKE "%' . $this->fullName . '%"' .
            'OR middleName LIKE "%' . $this->fullName . '%"'
        );

        return $dataProvider;
    }
}
