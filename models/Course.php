<?php

namespace app\models;

/**
 * This is the model class for table "courses".
 *
 * @property int $id
 * @property string $name
 * @property int $educator_id
 * @property bool $deleted
 *
 * @property Educator $educators
 * @property Group[] $groups
 */
class Course extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'courses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'educator_id'], 'required'],
            [['educator_id'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['educator_id'], 'exist', 'skipOnError' => true, 'targetClass' => Educator::className(), 'targetAttribute' => ['educator_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название курса',
            'educator_id' => 'Преподаватель',
            'fullName' => 'ФИО',
        ];
    }

    /**
     * Возвращает кол-во записей
     *
     * @param null $params
     * @return int|string
     */
    public static function getCount($params = null)
    {
        return static::find()->Where($params)->count();
    }

    /**
     * Возвращает ФИО
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->educators->getFullName();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEducators()
    {
        return $this->hasOne(Educator::className(), ['id' => 'educator_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroups()
    {
        return $this->hasMany(Group::className(), ['course_id' => 'id']);
    }
}
