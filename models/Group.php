<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "groups".
 *
 * @property int $id
 * @property string $name
 * @property int $course_id
 * @property bool $deleted
 *
 * @property Attendance[] $attendances
 * @property Course $courses
 * @property Student[] $students
 */
class Group extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'groups';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'course_id'], 'required'],
            [['course_id'], 'integer'],
            [['name'], 'string', 'max' => 45],
            [['course_id'], 'exist', 'skipOnError' => true, 'targetClass' => Course::className(), 'targetAttribute' => ['course_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название группы',
            'course_id' => 'Курс',
            'courseName' => 'Название курса',
        ];
    }

    /**
     * Возвращает кол-во записей
     *
     * @param null $params
     * @return int|string
     */
    public static function getCount($params = null)
    {
        return static::find()->Where($params)->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourses()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudents()
    {
        return $this->hasMany(Student::className(), ['group_id' => 'id']);
    }

    /**
     * Возвращает нащвание курса группы
     *
     * @return string
     */
    public function getCourseName()
    {
        return $this->courses->name;
    }

    /**
     * Задает группу студентам
     *
     * @param $arrayId
     * @param $value
     * @return bool
     * @throws \yii\db\Exception
     */
    public function setGroups($arrayId, $value)
    {
        if (!empty($arrayId)) {
            $query = "UPDATE students SET group_id = $value WHERE ";
            foreach ($arrayId as $id) {
                $query = $query . "user_id = $id OR ";
            }
            $query = $query . "user_id = ''";
            return Yii::$app->db->createCommand($query)->execute() ? true : false;
        }
        return true;
    }
}
