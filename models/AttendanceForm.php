<?php

namespace app\models;

use yii\base\Model;

class AttendanceForm extends Model
{
    /** @var integer */
    public $groupId;
    /** @var string */
    public $groupName;
    /** @var string */
    public $date;

    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->date = date('Y-m-d');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date', 'groupId'], 'required'],
            [['date', 'groupId', 'students'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'date' => 'Дата',
        ];
    }

    /**
     * Создает записи посещаемости
     *
     * @param $visitedStudents
     * @return bool
     */
    public function save($visitedStudents)
    {
        $group = Group::findOne($this->groupId);
        $studentsId = array_column($group->students, 'id');

        $students = array();

        foreach ($studentsId as $id) {
            if ($visitedStudents) {
                if (in_array($id, $visitedStudents)) {
                    array_push($students, array('id' => $id, 'visited' => true));
                    continue;
                }
            }
            array_push($students, array('id' => $id, 'visited' => false));
        }
        foreach ($students as $student) {
            try
            {
                $attendance = new Attendance();
                $attendance->student_id = $student['id'];
                $attendance->date = $this->date;
                $attendance->visited = $student['visited'];
                $attendance->save();
            } catch (\Exception $ex) {
                continue;
            }
        }
        return true;
    }
}
