<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Course;


/* @var $this yii\web\View */
/* @var $model app\models\Group */
/* @var $dataFreeStudents yii\data\ActiveDataProvider */

$this->title = 'Новая группа';
$this->params['breadcrumbs'][] = ['label' => 'Группы', 'url' => ['/groups']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="group-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="col-lg-5">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'course_id')->dropDownList(ArrayHelper::map(Course::find()->all(), 'id', function ($data) {
            return $data['name'];
        })) ?>

        <?= $this->render('_studentsList', [
            'dataProvider' => $dataFreeStudents,
            'update' => 'false',
            'listName' => 'students',
        ]) ?>

        <?= Html::submitButton('Создать группу', ['class' => 'btn btn-success']) ?>

        <?php ActiveForm::end(); ?>

    </div>

</div>
