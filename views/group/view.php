<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\data\ActiveDataProvider;

/* @var $this yii\web\View */
/* @var $model app\models\Group */
/* @var $dataGroupStudents ActiveDataProvider */

$this->title = $model->name;
Yii::$app->user->can('listGroups') ? $this->params['breadcrumbs'][] = ['label' => 'Группы', 'url' => ['index']] : null ;
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="group-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= $model->deleted ? Html::tag('h3', 'Группа удалена', ['style' => ['color' => 'red']])  : '' ?>
        <?= Yii::$app->user->can('updateGroup') ? (
            Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary'])
        ) : '' ?>
        <?= $model->deleted ? (
            Yii::$app->user->can('restoreGroup') ? (
                Html::a('Восстановить', ['restore'], [
                    'class' => 'btn btn-success',
                    'data' => [
                        'confirm' => 'Вы действительно хотите восстановить группу?',
                        'method' => 'post',
                        'params' => [
                            'id' => $model->id,
                        ],
                    ],
                ])
            ) : '') : (
            Yii::$app->user->can('deleteGroup') ? (
                Html::a('Удалить', ['delete'], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Вы действительно хотите удалить группу?',
                        'method' => 'post',
                        'params' => [
                            'id' => $model->id,
                        ],
                    ],
                ])
            ): '' ) ?>
        <?= Yii::$app->user->can('createAttendance', ['group' => $model, 'createAttendance' => true]) ? (
            Html::a('Отметить посещаемость', ['attendance/create'], [
                'class' => 'btn btn-success',
                'data' => [
                    'method' => 'post',
                    'params' => [
                        'groupId' => $model->id,
                    ],
                ],
            ])
        ) : '' ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            [
                'label' => 'Курс',
                'value' => Html::a($model->courses->name, Url::to(['course/view', 'id' => $model->course_id])),
                'format' => 'raw'
            ],
        ],
    ]) ?>

    <div>

        <h3>Студенты группы</h3>

        <?= $this->render('_studentsList', [
            'dataProvider' => $dataGroupStudents,
            'update' => false,
        ]) ?>

    </div>

</div>
