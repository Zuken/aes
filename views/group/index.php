<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\GroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Группы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="group-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Yii::$app->user->can('createGroup') ? (
            Html::a('Создать группу', ['create'], ['class' => 'btn btn-success'])
        ) : '' ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => function ($model) {
            if ($model->deleted)
            {
                return ['class'=>'danger'];
            }
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'name',
            'courseName',
            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'Действия',
                'headerOptions' => ['width' => '80'],
                'template' => (Yii::$app->user->can('viewGroup') ? '{view} ' : '')
                    . (Yii::$app->user->can('updateGroup') ? '{update} ' : ' ')
                    . (Yii::$app->user->can('deleteGroup') ? '{delete}' : ''),
                'buttons' => [
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', Url::to(['group/delete']), [
                            'data' => [
                                'confirm' => 'Вы действительно хотите удалить группу?',
                                'method' => 'post',
                                'params' => [
                                    'id' => $model->id,
                                ],
                            ],
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>

</div>
