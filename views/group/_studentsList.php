<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Group */
/* @var $form yii\widgets\ActiveForm */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $update boolean */
/* @var $listName string */

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'layout' => "{items}",
    'columns' => [
        [
            'class' => 'yii\grid\SerialColumn',
            'headerOptions' => ['width' => '30'],
        ],
        $update ? [
            'class' => 'yii\grid\CheckboxColumn',
            'checkboxOptions' => function ($model) {
                return ['value' => $model->user_id];
            },
            'name' => $listName,
            'headerOptions' => ['width' => '30'],
        ] : ['visible' => false],
        [
            'label' => 'ФИО',
            'value' => function ($model) {
                return Html::a($model->getFullName(), Url::to(['user/view', 'id' => $model->user_id]));
            },
            'format' => 'raw'
        ],
    ],
]);
