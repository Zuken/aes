<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Course;

/* @var $this yii\web\View */
/* @var $model app\models\Group */

/* @var $form yii\widgets\ActiveForm */

/* @var $dataFreeStudents yii\data\ActiveDataProvider */
/* @var $dataGroupStudents yii\data\ActiveDataProvider */

$this->title = 'Редактирование группы: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Группы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="group-update">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </p>

        <?php $form = ActiveForm::begin(); ?>

        <div class="col-lg-12">

            <div class="col-lg-5">

                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            </div>

            <div class="col-lg-5">

                <?= $form->field($model, 'course_id')->dropDownList(ArrayHelper::map(Course::find()->all(), 'id', function ($data) {
                    return $data['name'];
                })) ?>

            </div>

        </div>

        <div class="col-lg-12">

            <div class="col-lg-5">

                <h3>Студенты группы</h3>

                <?= $this->render('_studentsList', [
                    'dataProvider' => $dataGroupStudents,
                    'update' => true,
                    'listName' => 'studentGroup',
                ]) ?>

            </div>

            <div class="col-lg-5">

                <h3>Свободные студенты</h3>

                <?= $this->render('_studentsList', [
                    'dataProvider' => $dataFreeStudents,
                    'update' => true,
                    'listName' => 'newStudent',
                ]) ?>

            </div>

        </div>

    <?php ActiveForm::end(); ?>

</div>
