<?php

use yii\grid\GridView;
use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>

<?= Html::a('Создать', 'user/create', ['class' => 'btn btn-lg btn-success'])?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'rowOptions' => function ($data) {
        if ($data['deleted'])
        {
            return ['class'=>'danger'];
        }
    },
    'columns' => [
        [
            'class' => 'yii\grid\SerialColumn',
            'headerOptions' => ['width' => '30'],
        ],
        'fullName',
        [
            'attribute' => 'type',
            'value' => function ($data) {
                if ($data->type == 'student') {
                    return 'Студент';
                }
                else if ($data->type == 'educator') {
                    return 'Преподаватель';
                }
            },
            'filter' => Html::activeDropDownList(
                $searchModel,
                'type',
                [
                    'student' => 'Студенты',
                    'educator' => 'Преподаватели'
                ],
                [
                    'class'=>'form-control',
                    'prompt' => 'Все пользователи'
                ]
            ),
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'header'=>'Действия',
            'headerOptions' => ['width' => '80'],
            'template' => (Yii::$app->user->can('viewUser') ? '{view} ' : '')
                . (Yii::$app->user->can('updateUser') ? '{update} ' : ' ')
                . (Yii::$app->user->can('deleteUser') ? '{delete}' : ''),
            'buttons' => [
                'delete' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', Url::to(['user/delete']), [
                        'data' => [
                            'confirm' => 'Вы действительно хотите удалить пользователя?',
                            'method' => 'post',
                            'params' => [
                                'id' => $model->id,
                            ],
                        ],
                    ]);
                },
            ],
        ],
    ],
]) ?>
