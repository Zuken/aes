<?php

/* @var $userForm app\models\UserForm */
/* @var $this yii\web\View */

$this->title = 'Изменение пользователя: ' . $userForm->nowUsername;
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['/users']];
$this->params['breadcrumbs'][] = ['label' => $userForm->nowUsername, 'url' => ['view', 'id' => $userForm->id]];
$this->params['breadcrumbs'][] = 'Изменение';
?>
<div class="site-sign-up">
    <div class="row">
        <div class="col-lg-5">
            <?= $this->render('_form', [
                'userForm' => $userForm,
                'new' => false,
            ])?>
        </div>
    </div>
</div>
