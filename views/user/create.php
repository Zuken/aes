<?php

/* @var $userForm app\models\UserForm */
/* @var $this yii\web\View */

$this->title = 'Новый пользователь';
$this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['/users']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-sign-up">
    <div class="row">
        <div class="col-lg-5">
            <?= $this->render('_form', [
                'userForm' => $userForm,
                'new' => true,
            ])?>
        </div>
    </div>
</div>
