<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $userForm app\models\User */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $new boolean */
?>

<div class="user-form">
    <?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>

    <?= $form->field($userForm, 'nowUsername')->hiddenInput()->label(false) ?>

    <?= $form->field($userForm, 'username')->textInput(['autofocus' => true]) ?>

    <?= $form->field($userForm, 'password')->passwordInput() ?>

    <?= $form->field($userForm, 'repeatPassword')->passwordInput() ?>

    <?= $form->field($userForm, 'lastName')->textInput()?>

    <?= $form->field($userForm, 'firstName')->textInput()?>

    <?= $form->field($userForm, 'middleName')->textInput()?>

    <?= $new ? $form->field($userForm, 'type')->radioList(['student' => 'Студент', 'educator' => 'Преподаватель']) : $form->field($userForm, 'type')->hiddenInput()->label(false) ?>

    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary', 'name' => 'sign-up-button']) ?>

    <?php ActiveForm::end(); ?>
</div>
