<?php

/** @var $model app\models\User */
/** @var $dataProvider \yii\data\ActiveDataProvider */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\grid\GridView;

$this->title = $model->username;
Yii::$app->user->can('listUsers') ? $this->params['breadcrumbs'][] = ['label' => 'Пользователи', 'url' => ['/users']] : null;
$this->params['breadcrumbs'][] = $this->title;
?>

<p>
    <?= $model->deleted ? Html::tag('h3', 'Пользователь удален', ['style' => ['color' => 'red']])  : '' ?>
    <?= Yii::$app->user->can('updateUser', ['user' => $model]) ?
            Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) : '' ?>
    <?= $model->deleted ? (
        Yii::$app->user->can('restoreUser') ? (
            Html::a('Восстановить', 'restore', [
                'class' => 'btn btn-success',
                'data' => [
                    'confirm' => 'Вы действительно хотите восстановить пользователя?',
                    'method' => 'post',
                    'params' => [
                        'id' => $model->id,
                    ],
                ],
            ])
        ) : '' ) : (
        Yii::$app->user->can('deleteUser') ? (
            Html::a('Удалить', 'delete', [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Вы действительно хотите удалить пользователя?',
                    'method' => 'post',
                    'params' => [
                        'id' => $model->id,
                    ],
                ],
            ])
        ) : '' ) ?>
</p>
<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        'id',
        'username',
        [
            'label' => 'Тип пользователя',
            'value' => function ($model) {
                if ($model->type == 'student') {
                    return 'Студент';
                } else if ($model->type == 'educator') {
                    return 'Преподаватель';
                } else {
                    return 'Администратор';
                }
            },
        ],
        $model->type != 'admin' ? 'fullName' : ['visible' => false],
        $model->type == 'student' ? [
            'label' => 'Группа',
            'value' => $model->students->group_id ? Html::a($model->students->groups->name, Url::to(['group/view', 'id' => $model->students->group_id])) : '',
            'format' => 'raw'
        ] : ['visible' => false]
    ],
]) ?>

<?php if ($model->type == 'student') : ?>
    <?php if (Yii::$app->user->can('viewAttendance', ['user' => $model])) :?>
        <h3>Посещаемость</h3>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'date',
                [
                    'attribute' => 'visited',
                    'value' => function ($data) {
                        if ($data->visited) {
                            return 'Был';
                        }
                        return 'Не был';
                    },
                ],
            ]
        ]) ?>
    <?php endif; ?>
<?php endif; ?>
