<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Attendance */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Ред. посещаемости: ' . $model->getFullName();
$this->params['breadcrumbs'][] = ['label' => 'Посещаемость', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->getFullName(), 'url' => ['user/view', 'id' => $model->students->user_id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="attendance-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="col-lg-4">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'fullName')->hiddenInput()->label(false) ?>

        <?= $form->field($model, 'date')->widget(DatePicker::className(), [
            'name' => 'date',
            'type' => DatePicker::TYPE_COMPONENT_PREPEND,
            'value' => date('yyyy-mm-dd'),
            'readonly' => true,
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'autoclose' => true
            ]
        ]) ?>

        <?= $form->field($model, 'visited')->dropDownList(['0' => 'Не был', '1' => 'Был']) ?>

        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
