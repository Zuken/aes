<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\AttendanceForm */
/* @var $dataProvider \yii\data\ActiveDataProvider */

$this->title = "Посещаемость группы : $model->groupName" ;
$this->params['breadcrumbs'][] = ['label' => 'Посещаемость', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attendance-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="col-lg-5">

        <?php $form = ActiveForm::begin(['action' => ['save']]); ?>

        <?= $form->field($model, 'groupId')->hiddenInput()->label(false) ?>

        <?= $form->field($model, 'date')->widget(DatePicker::className(), [
            'name' => 'date',
            'type' => DatePicker::TYPE_COMPONENT_PREPEND,
            'readonly' => true,
            'pluginOptions' => [
                'todayHighlight' => true,
                'format' => 'yyyy-mm-dd',
                'autoclose' => true
            ]
        ]) ?>

        <?= \yii\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                [
                    'class' => 'yii\grid\SerialColumn',
                    'headerOptions' => ['width' => '30']
                ],
                'fullName',
                [
                    'class' => 'yii\grid\CheckboxColumn',
                    'name' => 'students',
                    'checkboxOptions' => function ($data) {
                        return ['value' => $data->id];
                    },
                    'headerOptions' => ['width' => '30']
                ]
            ],
        ]) ?>

        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
