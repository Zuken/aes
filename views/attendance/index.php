<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AttendanceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Посещаемость';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attendance-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'fullName',
            [
                'attribute' => 'date',
                'value' => 'date',
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'readonly' => true,
                    'attribute' => 'date',
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true,
                    ]
                ])
            ],
            [
                'attribute' => 'visited',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'visited',
                    [
                        '1' => 'Был',
                        '0' => 'Не был'
                    ],
                    [
                        'class'=>'form-control',
                        'prompt' => 'Не задано'
                    ]
                ),
                'value' => function ($data) {
                    if ($data->visited) {
                        return 'Был';
                    }
                    return 'Не был';
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['width' => '30'],
                'template' => '{update}'
            ],
        ],
    ]); ?>

</div>
