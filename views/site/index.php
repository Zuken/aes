<?php

/* @var $this yii\web\View */

$this->title = 'ДОУ: главная';
?>
<div class="site-index">

    <?= $this->render('_usersPanel') ?>

    <?= $this->render('_coursesPanel') ?>

    <?= $this->render('_groupsPanel') ?>

    <?= $this->render('_attendancesPanel') ?>

</div>
