<?php

use yii\widgets\DetailView;
use app\models\User;

?>

<div class="col-lg-3">
    <h3>Пользователи</h3>

    <?= DetailView::widget([
        'model' => User::className(),
        'attributes' => [
            [
                'label' => 'Кол-во пользователей',
                'value' => User::getCount(),
            ],
            [
                'label' => 'Студентов',
                'value' => User::getCount(['type' => User::STUDENT_TYPE]),
            ],
            [
                'label' => 'Преподавателей',
                'value' => User::getCount(['type' => User::EDUCATOR_TYPE]),
            ]
        ],
    ]) ?>
</div>
