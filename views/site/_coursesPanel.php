<?php

use yii\widgets\DetailView;
use app\models\Course;

?>

<div class="col-lg-3">
    <h3>Курсы</h3>

    <?= DetailView::widget([
        'model' => Course::className(),
        'attributes' => [
            [
                'label' => 'Кол-во курсов',
                'value' => Course::getCount(),
            ],
            [
                'label' => 'Кол-во активных курсов',
                'value' => Course::getCount(['deleted' => false]),
            ],
            [
                'label' => 'Кол-во неактивных курсов',
                'value' => Course::getCount(['deleted' => true]),
            ],
        ],
    ]) ?>
</div>
