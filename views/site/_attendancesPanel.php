<?php

use yii\widgets\DetailView;
use app\models\Attendance;

?>

<div class="col-lg-3">
    <h3>Посещаемость</h3>

    <?= DetailView::widget([
        'model' => Attendance::className(),
        'attributes' => [
            [
                'label' => 'Кол-во записей',
                'value' => Attendance::getCount(),
            ],
            [
                'label' => 'Не был',
                'value' => Attendance::getCount(['visited' => false]),
            ],
            [
                'label' => 'Был',
                'value' => Attendance::getCount(['visited' => true]),
            ],
        ],
    ]) ?>
</div>
