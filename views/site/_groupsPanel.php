<?php

use yii\widgets\DetailView;
use app\models\Group;

?>

<div class="col-lg-3">
    <h3>Группы</h3>

    <?= DetailView::widget([
        'model' => Group::className(),
        'attributes' => [
            [
                'label' => 'Кол-во групп',
                'value' => Group::getCount(),
            ],
            [
                'label' => 'Кол-во активных групп',
                'value' => Group::getCount(['deleted' => false]),
            ],
            [
                'label' => 'Кол-во неактивных групп',
                'value' => Group::getCount(['deleted' => true]),
            ],
        ],
    ]) ?>
</div>
