<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CourseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Курсы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Yii::$app->user->can('createCourse') ? Html::a('Создать курс', ['create'], ['class' => 'btn btn-success']) : '' ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => function ($model) {
            if ($model->deleted)
            {
                return ['class' => 'danger'];
            }
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'name',
            'fullName',
            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'Действия',
                'headerOptions' => ['width' => '80'],
                'template' => (Yii::$app->user->can('viewCourse') ? '{view} ' : '')
                    . (Yii::$app->user->can('updateCourse') ? '{update} ' : ' ')
                    . (Yii::$app->user->can('deleteCourse') ? '{delete}' : ''),
                'buttons' => [
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', Url::to(['course/delete']), [
                            'data' => [
                                'confirm' => 'Вы действительно хотите удалить курс?',
                                'method' => 'post',
                                'params' => [
                                    'id' => $model->id,
                                ],
                            ],
                        ]);
                    },
                ],
            ],
        ],
    ]); ?>


</div>
