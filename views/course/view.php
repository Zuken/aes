<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Course */

$this->title = $model->name;
Yii::$app->user->can('listCourses') ? $this->params['breadcrumbs'][] = ['label' => 'Курсы', 'url' => ['index']] : null;
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="course-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= $model->deleted ? Html::tag('h3', 'Курс удален', ['style' => ['color' => 'red']])  : '' ?>
        <?= Yii::$app->user->can('updateCourse') ? (
                Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary'])
            ) : '' ?>
        <?= $model->deleted ? (
            Yii::$app->user->can('restoreCourse') ? (
                Html::a('Восстановить', 'restore', [
                    'class' => 'btn btn-success',
                    'data' => [
                        'confirm' => 'Вы действительно хотите восстановить курс?',
                        'method' => 'post',
                        'params' => [
                            'id' => $model->id,
                        ],
                    ],
                ])
            ) : '') : (
            Yii::$app->user->can('deleteCourse') ? (
                Html::a('Удалить', 'delete', [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Вы действительно хотите удалить курс?',
                        'method' => 'post',
                        'params' => [
                            'id' => $model->id,
                        ],
                    ],
                ])
            ) : '' ) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            [
                'label' => 'Преподаватель',
                'value' => Html::a($model->educators->getFullName(), Url::to(['user/view', 'id' => $model->educators->user_id])),
                'format' => 'raw'
            ],
        ],
    ]) ?>

</div>
