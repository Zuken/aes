<?php

use yii\db\Migration;

/**
 * Handles the creation of table `attendance`.
 */
class m191219_152444_create_attendance_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('attendance', [
            'id' => $this->primaryKey(),
            'student_id' => $this->integer()->notNull(),
            'date' => $this->date()->notNull(),
            'visited' => $this->boolean()->notNull()
        ]);

        $this->addForeignKey(
            'FK_Attendance_Students',
            'attendance',
            'student_id',
            'students',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK_Attendance_Students', 'attendance');
        $this->dropTable('attendance');
    }
}
