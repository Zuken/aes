<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m191204_085219_create_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'username' => $this->string(20)->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password' => $this->string(256)->notNull(),
            'type' => $this->string()->notNull()->defaultValue('student'),
            'deleted' => $this->boolean()->notNull()->defaultValue(false)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('users');
    }
}
