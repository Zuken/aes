<?php

use yii\db\Migration;

/**
 * Handles the creation of table `courses`.
 */
class m191207_161825_create_courses_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('courses', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100)->notNull(),
            'educator_id' => $this->integer()->notNull(),
            'deleted' => $this->boolean()->notNull()->defaultValue(false)
        ]);

        $this->addForeignKey(
            'FK_Courses_Educators',
            'courses',
            'educator_id',
            'educators',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK_Curses_Educators', 'courses');
        $this->dropTable('courses');
    }
}
