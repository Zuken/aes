<?php

use yii\db\Migration;

/**
 * Handles the creation of table `educators`.
 */
class m191204_112539_create_educators_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('educators', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->unique()->notNull(),
            'group_id' => $this->integer(),
            'lastName' => $this->string(20)->notNull(),
            'firstName' => $this->string(20)->notNull(),
            'middleName' => $this->string(20)
        ]);

        $this->addForeignKey(
            'FK_Educators_Users',
            'educators',
            'user_id',
            'users',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK_Educators_Users', 'educators');
        $this->dropTable('educators');
    }
}
