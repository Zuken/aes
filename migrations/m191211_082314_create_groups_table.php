<?php

use yii\db\Migration;

/**
 * Handles the creation of table `groups`.
 */
class m191211_082314_create_groups_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('groups', [
            'id' => $this->primaryKey(),
            'name' => $this->string(45)->notNull(),
            'course_id' => $this->integer()->notNull(),
            'deleted' => $this->boolean()->notNull()->defaultValue(false)
        ]);

        $this->addForeignKey(
            'FK_Groups_Courses',
            'groups',
            'course_id',
            'courses',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK_Groups_Courses', 'groups');
        $this->dropTable('groups');
    }
}
