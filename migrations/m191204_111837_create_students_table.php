<?php

use yii\db\Migration;

/**
 * Handles the creation of table `students`.
 */
class m191204_111837_create_students_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('students', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->unique()->notNull(),
            'group_id' => $this->integer(),
            'lastName' => $this->string(20)->notNull(),
            'firstName' => $this->string(20)->notNull(),
            'middleName' => $this->string(20)
        ]);

        $this->addForeignKey(
            'FK_Students_Users',
            'students',
            'user_id',
            'users',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK_Students_Users', 'students');
        $this->dropTable('students');
    }
}
