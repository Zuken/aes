<?php
return [
    '/' => 'site/index',
    '<action:(login|logout)>' => 'site/<action>',

    'users' => 'user/index',
    'user/create' => 'user/create',
    'user/<id:\d+>' => 'user/view',
    'user/<id:\d+>/update' => 'user/update',
    'POST user/<action:(delete|restore)>' => 'user/<action>',

    'courses' => 'course/index',
    'course/create' => 'course/create',
    'course/<id:\d+>' => 'course/view',
    'course/<id:\d+>/update' => 'course/update',
    'POST course/<action:(delete|restore)>' => 'course/<action>',

    'groups' => 'group/index',
    'group/create' => 'group/create',
    'group/<id:\d+>' => 'group/view',
    'group/<id:\d+>/update' => 'group/update',
    'POST group/<action:(delete|restore)>' => 'group/<action>',

    'attendance' => 'attendance/index',
    'attendance/<id:\d+>/update' => 'attendance/update',
    'POST attendance/<action:(create|save)>' => 'attendance/<action>',
];
