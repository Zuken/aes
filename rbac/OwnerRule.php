<?php

namespace app\rbac;

use yii\rbac\Rule;

class OwnerRule extends Rule
{
    public $name = 'isOwner';

    public function execute($user, $item, $params)
    {
        if (isset($params['user'])) {
            return $params['user']->id == $user ? true : false;
        }
        if (isset($params['group'])) {
            if ($params['group']->courses->educators->user_id == $user) {
                return true;
            }
            if (isset($params['createAttendance'])) {
                return false;
            }
            $users = array_column($params['group']->students, 'user_id');
            if (in_array($user, $users)) {
                return true;
            }
            return false;
        }
        if (isset($params['attendance'])) {
            return $params['attendance']->students->groups->courses->educators->user_id == $user ? true : false;
        }
        return false;
    }
}