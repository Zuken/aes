<?php

namespace app\commands;

use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    /**
     * @throws \yii\base\Exception
     */
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        $rule = new \app\rbac\OwnerRule();
        $auth->add($rule);

        /** Permissions for Users */

        $listUsers = $auth->createPermission('listUsers');
        $listUsers->description = 'View list of users';
        $auth->add($listUsers);

        $viewUser = $auth->createPermission('viewUser');
        $viewUser->description = 'View a user';
        $auth->add($viewUser);

        $viewOwnUser = $auth->createPermission('viewOwnUser');
        $viewOwnUser->description = 'View own user';
        $viewOwnUser->ruleName = $rule->name;
        $auth->add($viewOwnUser);
        $auth->addChild($viewOwnUser, $viewUser);

        $createUser = $auth->createPermission('createUser');
        $createUser->description = 'Create user';
        $auth->add($createUser);

        $updateUser = $auth->createPermission('updateUser');
        $updateUser->description = 'Update user';
        $auth->add($updateUser);

        $updateOwnUser = $auth->createPermission('updateOwnUser');
        $updateOwnUser->description = 'Update own user';
        $updateOwnUser->ruleName = $rule->name;
        $auth->add($updateOwnUser);
        $auth->addChild($updateOwnUser, $updateUser);

        $deleteUser = $auth->createPermission('deleteUser');
        $deleteUser->description = 'Delete user';
        $auth->add($deleteUser);

        $restoreUser = $auth->createPermission('restoreUser');
        $restoreUser->description = 'Restore user';
        $auth->add($restoreUser);

        /** Permissions for Courses */

        $listCourses = $auth->createPermission('listCourses');
        $listCourses->description = 'View list of courses';
        $auth->add($listCourses);

        $viewCourse = $auth->createPermission('viewCourse');
        $viewCourse->description = 'View a course';
        $auth->add($viewCourse);

        $createCourse = $auth->createPermission('createCourse');
        $createCourse->description = 'Create course';
        $auth->add($createCourse);

        $updateCourse = $auth->createPermission('updateCourse');
        $updateCourse->description = 'Update course';
        $auth->add($updateCourse);

        $deleteCourse = $auth->createPermission('deleteCourse');
        $deleteCourse->description = 'Delete course';
        $auth->add($deleteCourse);

        $restoreCourse = $auth->createPermission('restoreCourse');
        $restoreCourse->description = 'Restore course';
        $auth->add($restoreCourse);

        /** Permissions for Groups */

        $listGroups = $auth->createPermission('listGroups');
        $listGroups->description = 'View list of groups';
        $auth->add($listGroups);

        $viewGroup = $auth->createPermission('viewGroup');
        $viewGroup->description = 'View a group';
        $auth->add($viewGroup);

        $viewOwnGroup = $auth->createPermission('viewOwnGroup');
        $viewOwnGroup->description = 'View own group';
        $viewOwnGroup->ruleName = $rule->name;
        $auth->add($viewOwnGroup);
        $auth->addChild($viewOwnGroup, $viewGroup);

        $createGroup = $auth->createPermission('createGroup');
        $createGroup->description = 'Create group';
        $auth->add($createGroup);

        $updateGroup = $auth->createPermission('updateGroup');
        $updateGroup->description = 'Update group';
        $auth->add($updateGroup);

        $deleteGroup = $auth->createPermission('deleteGroup');
        $deleteGroup->description = 'Delete group';
        $auth->add($deleteGroup);

        $restoreGroup = $auth->createPermission('restoreGroup');
        $restoreGroup->description = 'Restore group';
        $auth->add($restoreGroup);

        /** Permissions for Attendances */

        $listAttendances = $auth->createPermission('listAttendances');
        $listAttendances->description = 'View list of attendances';
        $auth->add($listAttendances);

        $viewAttendance = $auth->createPermission('viewAttendance');
        $viewAttendance->description = 'View a attendance';
        $auth->add($viewAttendance);

        $viewOwnAttendance = $auth->createPermission('viewOwnAttendance');
        $viewOwnAttendance->description = 'View own attendance';
        $viewOwnAttendance->ruleName = $rule->name;
        $auth->add($viewOwnAttendance);
        $auth->addChild($viewOwnAttendance, $viewAttendance);

        $createAttendance = $auth->createPermission('createAttendance');
        $createAttendance->description = 'Create attendance';
        $auth->add($createAttendance);

        $createOwnAttendance = $auth->createPermission('createOwnAttendance');
        $createOwnAttendance->description = 'Create own attendance';
        $createOwnAttendance->ruleName = $rule->name;
        $auth->add($createOwnAttendance);
        $auth->addChild($createOwnAttendance, $createAttendance);

        $updateAttendance = $auth->createPermission('updateAttendance');
        $updateAttendance->description = 'Update attendance';
        $auth->add($updateAttendance);

        $updateOwnAttendance = $auth->createPermission('updateOwnAttendance');
        $updateOwnAttendance->description = 'Update own attendance';
        $updateOwnAttendance->ruleName = $rule->name;
        $auth->add($updateOwnAttendance);
        $auth->addChild($updateOwnAttendance, $updateAttendance);

        /** Roles */

        $student = $auth->createRole('student');
        $auth->add($student);
        $auth->addChild($student, $viewOwnUser);
        $auth->addChild($student, $updateOwnUser);
        $auth->addChild($student, $viewOwnGroup);
        $auth->addChild($student, $viewOwnAttendance);

        $educator = $auth->createRole('educator');
        $auth->add($educator);
        $auth->addChild($educator, $listUsers);
        $auth->addChild($educator, $viewUser);
        $auth->addChild($educator, $listCourses);
        $auth->addChild($educator, $viewCourse);
        $auth->addChild($educator, $listGroups);
        $auth->addChild($educator, $viewGroup);
        $auth->addChild($educator, $listAttendances);
        $auth->addChild($educator, $viewAttendance);
        $auth->addChild($educator, $createOwnAttendance);
        $auth->addChild($educator, $updateOwnAttendance);

        $admin = $auth->createRole('admin');
        $auth->add($admin);
        // Users
        $auth->addChild($admin, $updateUser);
        $auth->addChild($admin, $createUser);
        $auth->addChild($admin, $deleteUser);
        $auth->addChild($admin, $restoreUser);
        // Courses
        $auth->addChild($admin, $updateCourse);
        $auth->addChild($admin, $createCourse);
        $auth->addChild($admin, $deleteCourse);
        $auth->addChild($admin, $restoreCourse);
        // Groups
        $auth->addChild($admin, $updateGroup);
        $auth->addChild($admin, $createGroup);
        $auth->addChild($admin, $deleteGroup);
        $auth->addChild($admin, $restoreGroup);
        // Attendance
        $auth->addChild($admin, $updateAttendance);
        $auth->addChild($admin, $createAttendance);
        // Roles
        $auth->addChild($admin, $student);
        $auth->addChild($admin, $educator);
    }
}
