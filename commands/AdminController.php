<?php

namespace app\commands;

use app\models\User;
use Yii;
use yii\console\Controller;

class AdminController extends Controller
{
    /**
     * @throws \yii\base\Exception
     */
    public function actionCreate()
    {
        $user = new User();
        $user->username = 'admin';

        $user->setPassword('password');
        $user->generateAuthKey();
        $user->type = 'admin';
        if ($user->save()) {
            $auth = Yii::$app->authManager;
            $role = $auth->getRole('admin');
            $auth->assign($role, $user->getId());
        }
    }
}
